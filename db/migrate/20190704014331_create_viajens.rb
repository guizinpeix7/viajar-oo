class CreateViajens < ActiveRecord::Migration[5.2]
  def change
    create_table :viajens do |t|
      t.string :modo
      t.float :duracao
      t.float :preco
      t.float :bagagemMax
      t.string :local
      t.references :agencia, foreign_key: true

      t.timestamps
    end
  end
end
