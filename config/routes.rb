Rails.application.routes.draw do
  
  devise_for :users, controllers:{
    registrations: 'registrations'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'agencias#index', as: 'home'
  resources:agencias do 
            resources:viajens
  end                   
  
  #Essa linha de codigo define previamente todas as rotas dentro do site 
  #Sendo assim o usuario necessita somente indicar no controller o redirecionamento desejado
  get 'listaAgencias' => 'agencias#listaAgencias' , as: 'listaagencias'
  
  get 'create' => 'agencias#new', as: 'neew' #Agencias#new se refere ao metodo que esta dentro da minhas controller de agencias
  match 'ViajenCreate' => 'viajens#ccre' , via: 'get' 

end


#match 'inicio' => 'restaurantes#index', via: 'get'
#Nesse exemplo o método match recebeu um hash onde a primeira chave é 'inicio' (rota a ser mapeada)
# e o valor para essa chave é 'restaurantes#index', ou seja, quando acessar localhost:3000/inicio
# o Rails vai executar a action index no controller restaurantes.