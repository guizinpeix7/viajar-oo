class AgenciasController < ApplicationController
    http_basic_authenticate_with name: "admin", password:"admin", except: [:index, :show]   
    
    def index
        @agencias = Agencia.all
    end
    def listaAgencias
        @agencias = Agencia.all
    end

    def show 
        @agencia = Agencia.find(params[:id])
    end
    def new
        @agencia = Agencia.new
    end
    def create
        #@render plain: params[:agencia].inspect
        @agencia = Agencia.new(agencia_params) 
        if(@agencia.save)
            redirect_to listaagencias_path listaAgencias
        else 
            render 'new'
        end
    end

    def edit 
        puts params
        @agencia = Agencia.find(params[:id])
    end

    def update
        @agencia = Agencia.find(params[:id])
        if(@agencia.update(agencia_params))
            redirect_to listaagencias_path
        else
            render 'edit'
        end
    end
    
    def destroy  
        @agencia = Agencia.find(params[:id])
        @agencia.destroy
        redirect_to listaagencias_path
    end

        

    private def agencia_params
        params.require(:agencia).permit(:nome)
    end
end
