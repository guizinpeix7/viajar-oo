class Agencia < ApplicationRecord
    has_many :viajens 
    validates :nome, presence: true, 
                     length: {minimum: 3}
     
end
