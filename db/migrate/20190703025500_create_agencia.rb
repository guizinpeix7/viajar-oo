class CreateAgencia < ActiveRecord::Migration[5.2]
  def change
    create_table :agencia do |t|
      t.string :nome

      t.timestamps
    end
  end
end
